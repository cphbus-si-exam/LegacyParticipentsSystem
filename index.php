<?php

include "vendor/autoload.php";

use benjaco\Highway\Highway;
use Mailgun\Mailgun;

$con = new mysqli(
    "localhost",
    "phpuser",
    "pass1234",
    "event"
);

// setup can be called if you dont want the default parameter, or Highway::$url can be set
// Highway::set_up(false);
Highway::set_up(false);

Highway::get("/", function () {
    echo "legacy server";
});


Highway::get("/event/{}/participant",  function($event){
    global $con;
    $stmt = $con->prepare("select * from participants where event_id = ?");
    if (!$stmt) {
        echo "Failed to connect to MySQL: (" . $con->connect_errno . ") " . $con->connect_error;
    }
    $stmt->bind_param("s", $event);
    $stmt->execute();
    $stmt->bind_result($id,
        $name,
        $email,
        $event_id,
        $replied,
        $participating);

    $results = ["participants"=>[]];

    while ($stmt->fetch()) {
        $results["participants"][]=[
            "id"=>$id,
            "name"=>$name,
            "email"=>$email,
            "event_id"=>$event_id,
            "replied"=>$replied,
            "participating"=>$participating,
        ];
    }
    $stmt->close();


    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    echo json_encode($results);
});


Highway::post("/event/{}/participant",  function($event){
    global $con;
    header('Content-Type: application/json');

    if (
        !is_string($event)
    ) {
        header("HTTP/1.1 400 Bad Request");
            echo json_encode(["status"=>"failed", "error" => "Not valid event id"]);
        return;
    }
    if (
        !isset($_POST["participants"]) ||  !is_array($_POST["participants"])
    ) {
        header("HTTP/1.1 400 Bad Request");
            echo json_encode(["status"=>"failed", "error" => "Not valid participant issnt an array"]);
        return;
    }
    if (
        !isset($_POST["sendinvite"])
    ) {
        header("HTTP/1.1 400 Bad Request");
            echo json_encode(["status"=>"failed", "error" => "Not valid send invite, not a bool"]);
        return;
    }
    $_POST["sendinvite"]=in_array($_POST["sendinvite"], [1, "on", "true"]) ;
    foreach ($_POST["participants"] as $item){
        if(
            !isset($item["name"]) || !is_string($item["name"]) ||
            !isset($item["email"]) || !is_string($item["email"])
        ){
            header("HTTP/1.1 400 Bad Request");
                echo json_encode(["status"=>"failed", "error" => "Not valid participants, each participant must have an email and a name defined as strings"]);
            return;
        }
    }

    foreach ($_POST["participants"] as $item) {
        $stmt = $con->prepare("insert into participants (`name`, email, event_id, replied, participating) values (?,?,?,0, null)");
        $name = $item["name"];
        $email = $item["email"];
        $stmt->bind_param("sss", $name, $email, $event);
        $stmt->execute();
        $stmt->close();
    }

    if($_POST["sendinvite"] === true){

        $mgClient = Mailgun::create('c18b01d0767fb060564200f4ad343390-5645b1f9-e8b1e654');
        $domain = "sandboxe6f7dc0dbd284c5497252b4ca28eed8b.mailgun.org";

        foreach ($_POST["participants"] as $item) {
            $result = $mgClient->messages()->send("$domain",
                array('from'    => 'Mailgun Sandbox <postmaster@sandboxe6f7dc0dbd284c5497252b4ca28eed8b.mailgun.org>',
                    'to'      => $item["email"],
                    'subject' => 'Hello '.$item["name"],
                    'text'    => 'Hello '.$item["name"]."
           
You have been invited to an event!

I am coming: http://167.172.109.112/accept/".$event."/".$item["email"]."

I can not come: http://167.172.109.112/reject/".$event."/".$item["email"]
                ));
        }
    }

    header("HTTP/1.1 201 Created");
    echo json_encode(["status"=>"ok"]);
});

Highway::get("/accept/{}/{}", function ($event, $email){
    global $con;
    $stmt = $con->prepare("update participants set replied = 1, participating = 1 where event_id = ? and email = ?");
    $stmt->bind_param("ss", $event, $email);
    $stmt->execute();
    $stmt->close();
    echo "Thank you for the reply";
});
Highway::get("/reject/{}/{}", function ($event, $email){
    global $con;
    $stmt = $con->prepare("update participants set replied = 1, participating = 0 where event_id = ? and email = ?");
    $stmt->bind_param("ss", $event, $email);
    $stmt->execute();
    $stmt->close();
    echo "Thank you for the reply";
});

Highway::not_found(function () {
    echo 404;
});